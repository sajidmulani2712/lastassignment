import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'assignment';

  randomNumber:any;

  random(min:any, max:any) {
    min = Math.ceil(min);
    max = Math.floor(max);
    this.randomNumber =  Math.floor(Math.random() * (max - min) + min); 

    return this.randomNumber
  }
}
