import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  randomNumber:any;

  constructor() { }

  ngOnInit(): void {
  }



  random(min:any, max:any) {
    min = Math.ceil(min);
    max = Math.floor(max);
    this.randomNumber =  Math.floor(Math.random() * (max - min) + min); 

    return this.randomNumber
  }

}
